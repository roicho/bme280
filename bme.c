/*
 * bme.c
 *
 *  Created on: 29 de ago. de 2018
 *      Author: Roi
 */

#include "bme.h"
#include "bme280.h"
#include "string.h"
#include "stdlib.h"
#include "stm32f1xx_hal.h"

#define TEMPERATURE_TO_CELSIUS	100U
#define PRESSURE_TO_HPAS		100U
#define HUMIDITY_TO_PERCENT		1024U
#define BME_TIMEOUT_1_SEC		1000U

static I2C_HandleTypeDef * i2c1_p;
static struct bme280_dev bme_config;

static void ConvertToString(int number_int, int number_dec, char * output_string);

int8_t user_i2c_read(uint8_t id, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
	HAL_I2C_Master_Transmit(i2c1_p, (uint16_t) id, &reg_addr, sizeof(reg_addr), BME_TIMEOUT_1_SEC);
	HAL_I2C_Master_Receive(i2c1_p, (uint16_t) id, data, len, BME_TIMEOUT_1_SEC);
	return 0;
}

int8_t user_i2c_write(uint8_t id, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
	uint8_t *buf;
	buf = malloc(len +1);
	buf[0] = reg_addr;
	memcpy(buf +1, data, len);

	HAL_I2C_Master_Transmit(i2c1_p, (uint16_t) id, buf, (len+1), BME_TIMEOUT_1_SEC);
	free(buf);
	return 0;
}


bme_result_T BmeInit(I2C_HandleTypeDef * i2c1)
{
	int8_t rslt = BME280_E_DEV_NOT_FOUND;
	bme_result_T result = BME_ERROR;
	uint8_t settings_sel;

	i2c1_p = i2c1;

	bme_config.dev_id = 0xEC;
	bme_config.intf = BME280_I2C_INTF;
	bme_config.read = user_i2c_read;
	bme_config.write = user_i2c_write;
	bme_config.delay_ms = HAL_Delay;

	rslt = bme280_init(&bme_config);
	if(rslt == BME280_OK)
	{
		/* Recommended mode of operation: Indoor navigation */
		bme_config.settings.osr_h = BME280_OVERSAMPLING_1X;
		bme_config.settings.osr_p = BME280_OVERSAMPLING_16X;
		bme_config.settings.osr_t = BME280_OVERSAMPLING_2X;
		bme_config.settings.filter = BME280_FILTER_COEFF_16;
		bme_config.settings.standby_time = BME280_STANDBY_TIME_1000_MS;

		settings_sel = BME280_OSR_PRESS_SEL;
		settings_sel |= BME280_OSR_TEMP_SEL;
		settings_sel |= BME280_OSR_HUM_SEL;
		settings_sel |= BME280_STANDBY_SEL;
		settings_sel |= BME280_FILTER_SEL;
		rslt = bme280_set_sensor_settings(settings_sel, &bme_config);
		rslt = bme280_set_sensor_mode(BME280_NORMAL_MODE, &bme_config);
	}

	if(rslt == BME280_OK)
	{
		result = BME_OK;
	}
	else
	{
		result = BME_ERROR;
	}

	return result;
}

static void ConvertToString(int number_int, int number_dec, char * output_string)
{
	char auxiliar_string[8];

	itoa(number_int, auxiliar_string, 10);
	strcpy(output_string, auxiliar_string);

	strcat(output_string, ",");

	itoa(number_dec, auxiliar_string, 10);
	auxiliar_string[2] = 0;
	if(auxiliar_string[1] == 0)
	{
		auxiliar_string[1] = '0';
	}
	strcat(output_string, auxiliar_string);
}

bme_result_T BmeGetData(bme_data_T * bme_data)
{
	struct bme280_data comp_data;
	int8_t rslt = BME280_E_DEV_NOT_FOUND;
	bme_result_T result = BME_ERROR;

	rslt = bme280_get_sensor_data(BME280_ALL, &comp_data, &bme_config);

	if(rslt == BME280_OK)
	{
		bme_data->temp_celsius_int = comp_data.temperature / TEMPERATURE_TO_CELSIUS;
		bme_data->temp_celsius_dec = abs(comp_data.temperature % TEMPERATURE_TO_CELSIUS);
		bme_data->temperature = comp_data.temperature;
		ConvertToString(bme_data->temp_celsius_int, bme_data->temp_celsius_dec, bme_data->temp_string);

		bme_data->hum_perc_int = comp_data.humidity / HUMIDITY_TO_PERCENT;
		bme_data->hum_perc_dec = comp_data.humidity % HUMIDITY_TO_PERCENT;
		bme_data->humidity = comp_data.humidity;
		ConvertToString(bme_data->hum_perc_int, bme_data->hum_perc_dec, bme_data->hum_string);

		bme_data->press_hpas_int = comp_data.pressure / PRESSURE_TO_HPAS;
		bme_data->press_hpas_dec = comp_data.pressure % PRESSURE_TO_HPAS;
		bme_data->pressure = comp_data.pressure;
		ConvertToString(bme_data->press_hpas_int, bme_data->press_hpas_dec, bme_data->press_string);

		result = BME_OK;
	}
	else
	{
		result = BME_ERROR;
	}

	return result;
}
