/*
 * bme.h
 *
 *  Created on: 29 de ago. de 2018
 *      Author: Roi
 */

#ifndef BME_H_
#define BME_H_

#include "stm32f1xx_hal.h"

typedef enum
{
	BME_OK = 0,
	BME_ERROR = 1
}bme_result_T;

typedef struct
{
	int8_t temp_celsius_int;
	uint8_t temp_celsius_dec;
	char temp_string[6];
	int32_t temperature;
	uint8_t hum_perc_int;
	uint8_t hum_perc_dec;
	char hum_string[6];
	uint32_t humidity;
	uint16_t press_hpas_int;
	uint8_t press_hpas_dec;
	char press_string[8];
	uint32_t pressure;
}bme_data_T;

bme_result_T BmeInit(I2C_HandleTypeDef * i2c1);
bme_result_T BmeGetData(bme_data_T * bme_data);

#endif /* BME_H_ */
